# Patch Generator #

## Description ##

Generates patch dataset based on aligned RGB and NIR rasters.

## Pre-requisities ##

* OpenCV (python)
* Tkinter
* gdal
* Image, ImageTk

## Run ##
`python patch-generator.py`
