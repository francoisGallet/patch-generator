# pre requisites
# ---------------
# - gdal python library (should be installed if qGis is installed on machine)
# - openCV python 2.4.9 or beyond

import gdal
from gdalconst import *
from math import radians, cos, sin, asin, sqrt, atan2, pi
import cv2

ZONE_SIZE_METER = 8

import numpy as np

# Function list
# ---------------

# gets raster info
def get_raster_info(input_raster):
    # dimensions
    nrows = input_raster.RasterYSize
    ncols = input_raster.RasterXSize

    # compute x and y scales (px to meter) from metadata
    geotransform = input_raster.GetGeoTransform()
    long_scale = np.abs(geotransform[1])
    lat_scale = np.abs(geotransform[5])
    long_upleft = geotransform[0]
    lat_upleft = geotransform[3]

    # return result
    return nrows, ncols, long_upleft, lat_upleft, long_scale, lat_scale


# streches channel to 8bit scale
def strech_8bit(channel):
    ch_max = np.max(channel)
    ch_min = np.min(channel)
    if ch_max == 0:
        return channel.astype('uint8')
    else:
        return ((channel - ch_min) * 255.0 / (ch_max - ch_min)).astype('uint8')


# get pixel coordinates from GPS coordinates and scales
def getPix(origin, dest, long_scale, lat_scale):
    return int((dest[0] - origin[0]) / long_scale), int((origin[1] - dest[1]) / lat_scale) # lat is going down

# gets block from pix coordinates and raster bands
def get_block(x, y, x_size, y_size, red_band, green_band, blue_band):
    # if DBG=='1':
        # print 'getting block {},{} - size {}|{}'.format(x ,y, x_size, y_size)
    r = red_band.ReadAsArray(x, y, x_size, y_size)
    g = green_band.ReadAsArray(x, y, x_size, y_size)
    b = blue_band.ReadAsArray(x, y, x_size, y_size)
    return [r,g,b]

# extract channels from block
def get_img(block):
    return cv2.merge((block[0], block[1], block[2]))

# compute distance between two gps points in m
# reference : http://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points
def haversine(lon1, lat1, lon2, lat2):
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371008 # Radius of earth in meters
    return c * r


class ZoneService:

    def __init__(self):
        self.rgbPath = ""
        self.nirPath = ""
        self.rgbRaster = None
        self.nirRaster = None
        self.rgbZone = None
        self.nirZone = None
        self.scaleRgb2NirX = 1.0
        self.scaleRgb2NirY = 1.0

    def rgbGetPatch(self, x, y):
        if not self.rgbZone is None:
            x_center = int(x * self.rgbZone.shape[0])
            y_center = int(y * self.rgbZone.shape[1])
            return self.rgbZone[x_center-32:x_center+32, y_center-32:y_center+32]

    def nirGetPatch(self, x, y):
        if not self.nirZone is None:
            x_center = int(x * self.nirZone.shape[0])
            y_center = int(y * self.nirZone.shape[1])

            x_pix = int(32 / self.scaleRgb2NirX)
            y_pix =  int(32 / self.scaleRgb2NirY)

            patch = self.nirZone[x_center-x_pix:x_center+x_pix, y_center-y_pix:y_center+y_pix]
            patch = cv2.resize(patch, (64,64))

            return patch


    def refreshZone(self, rgbPath, nirPath):
        # open rasters
        if rgbPath != self.rgbPath:
            self.rgbPath = rgbPath
            self.rgbRaster =  gdal.Open(rgbPath, GA_ReadOnly)

        if nirPath != self.nirPath:
            self.nirPath = nirPath
            self.nirRaster =  gdal.Open(nirPath, GA_ReadOnly)

        # grab bands
        rgb_red_rastaband = self.rgbRaster.GetRasterBand(1)
        rgb_green_rastaband = self.rgbRaster.GetRasterBand(2)
        rgb_blue_rastaband = self.rgbRaster.GetRasterBand(3)

        nir_red_rastaband = self.nirRaster.GetRasterBand(1)
        nir_green_rastaband = self.nirRaster.GetRasterBand(2)
        nir_blue_rastaband = self.nirRaster.GetRasterBand(3)

        # get raster info
        rgb_nrows, rgb_ncols, rgb_long_upleft, rgb_lat_upleft, rgb_long_scale, rgb_lat_scale = get_raster_info(self.rgbRaster)
        nir_nrows, nir_ncols, nir_long_upleft, nir_lat_upleft, nir_long_scale, nir_lat_scale = get_raster_info(self.nirRaster)

        rgb_origin = [rgb_long_upleft, rgb_lat_upleft]
        nir_origin = [nir_long_upleft, nir_lat_upleft]

        # compute meter to degree scale (since both rasters are located in the same area, just use RGB values as reference)
        meters2longDeg = haversine(rgb_long_upleft, rgb_lat_upleft, rgb_long_upleft + 1.0, rgb_lat_upleft)
        meters2latDeg = haversine(rgb_long_upleft, rgb_lat_upleft, rgb_long_upleft, rgb_lat_upleft + 1.0)

        # compute block size in long, lat
        block_size_long = ZONE_SIZE_METER * 1.0 / meters2longDeg
        block_size_lat = ZONE_SIZE_METER * 1.0 / meters2latDeg

        # compute pixel size for each raster
        rgb_X_pix2meter = np.ceil(1.0 / (meters2longDeg * rgb_long_scale))
        rgb_Y_pix2meter = np.ceil(1.0 / (meters2latDeg * rgb_lat_scale))
        nir_X_pix2meter = np.ceil(1.0 / (meters2longDeg * nir_long_scale))
        nir_Y_pix2meter = np.ceil(1.0 / (meters2latDeg * nir_lat_scale))
        # print '1 meter = {}x{}pix RGB, {}x{}pix NIR'.format(rgb_X_pix2meter, rgb_Y_pix2meter, nir_X_pix2meter, nir_Y_pix2meter)

        # compute block sizes
        rgb_block_Xsize = int(rgb_X_pix2meter * ZONE_SIZE_METER)
        rgb_block_Ysize = int(rgb_Y_pix2meter * ZONE_SIZE_METER)

        nir_block_Xsize = int(nir_X_pix2meter * ZONE_SIZE_METER)
        nir_block_Ysize = int(nir_Y_pix2meter * ZONE_SIZE_METER)

        # compute RGB to NIR X and Y scales
        rgb2nir_Xscale = rgb_X_pix2meter * 1.0 / nir_X_pix2meter
        rgb2nir_Yscale = rgb_Y_pix2meter * 1.0 / nir_Y_pix2meter

        self.scaleRgb2NirX = rgb2nir_Xscale
        self.scaleRgb2NirY = rgb2nir_Yscale

        # compute bounds of the area of common inclusion of both rasters
        ## compute effective ranges for lat and long for each raster (ie minus box size on lower right)
        error_margin = 20
        rgb_long_eff_range = (rgb_ncols - error_margin) * rgb_long_scale - block_size_long
        rgb_lat_eff_range = (rgb_nrows - error_margin) * rgb_lat_scale - block_size_lat # positive - will need to subtract it !
        nir_long_eff_range = (nir_ncols - error_margin) * nir_long_scale - block_size_long
        nir_lat_eff_range = (nir_nrows - error_margin) * nir_lat_scale - block_size_lat # positive - will need to subtract it !
        ## if rasters don't overlap, raise error and quit
        if (rgb_long_upleft > nir_long_upleft + nir_long_eff_range) or (nir_long_upleft > rgb_long_upleft + rgb_long_eff_range):
            print 'Error: raster longitudes do not overlap - aborting'
            quit()
        elif (rgb_lat_upleft < nir_lat_upleft - nir_lat_eff_range) or (nir_lat_upleft < rgb_lat_upleft - rgb_lat_eff_range):
            print 'Error: raster latitudes do not overlap - aborting'
            quit()
        else:
            # conpute common area
            common_long_min = max(rgb_long_upleft, nir_long_upleft)
            common_lat_max = min(rgb_lat_upleft, nir_lat_upleft)
            common_long_max = min(rgb_long_upleft + rgb_long_eff_range, nir_long_upleft + nir_long_eff_range)
            common_lat_min = max(rgb_lat_upleft - rgb_lat_eff_range, nir_lat_upleft - nir_lat_eff_range)
            common_long_range = common_long_max - common_long_min
            common_lat_range = common_lat_max - common_lat_min


        # select randomly GPS coordinates within the RGB
        found = False

        while not found:

            ## pick a point
            point = [common_long_min + np.random.rand() * common_long_range, common_lat_min + np.random.rand() * common_lat_range]

            # get block data
            # get data from RGB raster
            rgb_X,rgb_Y = getPix(rgb_origin, point, rgb_long_scale, rgb_lat_scale)

            rgb_block = get_block(rgb_X, rgb_Y, rgb_block_Xsize, rgb_block_Ysize, rgb_red_rastaband, rgb_green_rastaband, rgb_blue_rastaband)

            # get data from NIR raster
            nir_X,nir_Y = getPix(nir_origin, point, nir_long_scale, nir_lat_scale)

            nir_block = get_block(nir_X, nir_Y, nir_block_Xsize, nir_block_Ysize, nir_red_rastaband, nir_green_rastaband, nir_blue_rastaband)

            ## if both blocks are not empty, add them to the list
            if (np.sum(rgb_block) != 0) and (np.sum(nir_block) != 0):
                found = True

                self.rgbZone = get_img(rgb_block)
                self.nirZone = get_img(nir_block)
