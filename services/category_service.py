import sys, os
import xml.etree.cElementTree as ET
import cv2

CATEGORY_FOLDER = 'categories'

class Category:

    def __init__(self, name, service):
        self.name = name
        self.service = service

        self.dir = CATEGORY_FOLDER + '/' + self.name
        os.system('mkdir -p ' + self.dir)

        self.logFile = XmlLog(self.dir + '/log.xml')
        self.patchCount = self.logFile.patchCount

    def setButton(self, button):
        self.button = button

    def refreshButtonText(self):
        self.button.configure(text=self.name + ' (' + str(self.patchCount) + ')')

    def addPatch(self):
        if not self.service.root.rgbPatch is None:

            rgbPatch = self.service.root.rgbPatch
            nirPatch = self.service.root.nirPatch

            self.service.root.resetPatch()

            self.patchCount += 1
            self.logFile.patchCount = self.patchCount
            self.logFile.write()
            self.refreshButtonText()
            filePrefix = format(self.patchCount, '05d')

            cv2.imwrite(self.dir + '/' + filePrefix + '_rgb.tif', cv2.cvtColor(rgbPatch, cv2.COLOR_RGB2BGR))
            cv2.imwrite(self.dir + '/' + filePrefix + '_nir.tif', cv2.cvtColor(nirPatch, cv2.COLOR_RGB2BGR))


class CategoryService():

    def __init__(self):
        self.root = None

        self.categories = []

        self.categories.append(Category('non-vege', self))
        self.categories.append(Category('sol-nu', self))
        self.categories.append(Category('herbe', self))
        self.categories.append(Category('vigne-saine', self))
        self.categories.append(Category('vigne-malade', self))


class XmlLog:

    def __init__(self, path):

        self.path = path
        self.patchCount = 0

        if not os.path.isfile(path):
            self.write()
        else:
            self.patchCount = self.read()

    def write(self):
        root = ET.Element("root")

        subEle = ET.SubElement(root, "patch_count")
        root.set("patch_count", str(self.patchCount))

        tree = ET.ElementTree(root)
        tree.write(self.path)

    def read(self):
        tree = ET.parse(self.path)
        root = tree.getroot()
        return int(root.get("patch_count"))
