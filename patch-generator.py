from tkinter import *
import Image, ImageTk
from services.category_service import *
from services.zone_service import *

SCRIPT_VERSION = "0.01"
SCRIPT_AUTHOR = "Francois GALLET"

APP_WIDTH = 100
ZONE_SIZE = 600

# user interface
class userInterface:

    def __init__(self, app, zoneService, categoryService):
        self.app = app
        self.zoneService = zoneService
        self.categoryService = categoryService
        categoryService.root = self

        self.setup()

    def setup(self):
        self.app.title("Agro Calculator v" + str(SCRIPT_VERSION))

        self.frmHeader = Frame(self.app)

        Label(self.frmHeader, text="Agro Calculator").pack()
        Label(self.frmHeader, text="v {} - {}".format(SCRIPT_VERSION, SCRIPT_AUTHOR)).pack()

        self.rgbRasterPath = StringVar()
        self.rgbRasterPath.set('/home/francois/dro/proc/DATA/test-data/buemilsb1_1011_vis/buemilsb1_1011_vis_aligned.tif')
        self.inpRgbPath = Entry(self.frmHeader, textvariable=self.rgbRasterPath, width=APP_WIDTH)
        self.inpRgbPath.pack()

        self.nirRasterPath = StringVar()
        self.nirRasterPath.set('/home/francois/dro/proc/DATA/test-data/buemilsb1_1011_nir_aligned/buemilsb1_1011_nir_aligned_calib.tif')
        self.inpNirPath = Entry(self.frmHeader, textvariable=self.nirRasterPath, width=APP_WIDTH)
        self.inpNirPath.pack()

        self.frmHeader.pack()

        self.frmWorkspace = Frame(self.app)

        self.cvaZoneDisplay = Canvas(self.frmWorkspace, width=ZONE_SIZE, height=ZONE_SIZE, background='black')

        self.cvaZoneDisplay.bind("<Button-1>", self.zoneClick)

        self.cvaZoneDisplay.pack(side=LEFT)

        self.frmLeftPane = Frame(self.frmWorkspace)

        self.btnRefreshZone = Button(self.frmLeftPane, text='refresh \n zone', command=self.refreshZone)
        self.btnRefreshZone.pack()

        self.cvaPatchRgb = Canvas(self.frmLeftPane, width=64, height=64, background='black')
        self.cvaPatchRgb.pack()

        self.cvaPatchNir = Canvas(self.frmLeftPane, width=64, height=64, background='black')
        self.cvaPatchNir.pack()

        self.frmCategories = Frame(self.frmLeftPane)
        self.btnCategories = []

        for cat in self.categoryService.categories:
            btnCategory = Button(self.frmCategories, command=cat.addPatch, width=APP_WIDTH/6)
            cat.setButton(btnCategory)
            cat.refreshButtonText()
            self.btnCategories.append(btnCategory)
            btnCategory.pack()

        self.frmCategories.pack()

        self.frmLeftPane.pack(side=RIGHT)

        self.frmWorkspace.pack()

        self.frmBottomPane = Frame(self.app, width=APP_WIDTH)

        Button(self.frmBottomPane, text="Quitter", command=self.app.quit).pack()

        self.frmBottomPane.pack()

    def updateCatergoryButtons(self):
        for btnCategory in self.btnCategories:
            category = self.CategoryService


    def zoneClick(self, event):
        x = event.x
        y = event.y

        if not self.zoneService.rgbZone is None:

            # print "cliked at {}, {}".format(event.x, event.y)
            self.rgbPatch = self.zoneService.rgbGetPatch(y * 1.0 / ZONE_SIZE, x  * 1.0 / ZONE_SIZE)

            self.zonePatchRgb = Image.fromarray(self.rgbPatch)
            self.patchImgtkRgb = ImageTk.PhotoImage(image=self.zonePatchRgb)

            self.patchImgCvaRgb = self.cvaPatchRgb.create_image(0, 0, image=self.patchImgtkRgb, anchor="nw")


            self.nirPatch = self.zoneService.nirGetPatch(y  * 1.0 / ZONE_SIZE, x  * 1.0 / ZONE_SIZE)

            r, g, b = cv2.split(self.nirPatch)
            self.nirPatchForDislpay = strech_8bit(g)

            self.zonePatchNir = Image.fromarray(self.nirPatchForDislpay)
            self.patchImgtkNir = ImageTk.PhotoImage(image=self.zonePatchNir)

            self.patchImgCvaNir = self.cvaPatchNir.create_image(0, 0, image=self.patchImgtkNir, anchor="nw")


    def resetPatch(self):
        self.rgbPatch = None
        self.zonePatchRgb = None
        self.patchImgtkRgb = None
        self.patchImgCvaRgb = self.cvaPatchRgb.create_image(0, 0, image=None, anchor="nw")

        self.nirPatch = None
        self.zonePatchNir = None
        self.patchImgtkNir = None
        self.patchImgCvaNir = self.cvaPatchNir.create_image(0, 0, image=None, anchor="nw")

    def refreshZone(self):
        self.zoneService.refreshZone(self.rgbRasterPath.get(),self.nirRasterPath.get())

        self.zoneImg = Image.fromarray(self.zoneService.rgbZone)
        self.zoneImgtk = ImageTk.PhotoImage(image=self.zoneImg.resize((ZONE_SIZE,ZONE_SIZE)))

        self.zoneImgCva = self.cvaZoneDisplay.create_image(0, 0, image=self.zoneImgtk, anchor="nw")

        self.resetPatch()

    def run(self):
        self.app.mainloop()



if __name__ == '__main__':

    zoneService = ZoneService()

    categoryService = CategoryService()

    app = Tk()
    ui = userInterface(app, zoneService, categoryService)

    ui.run()
